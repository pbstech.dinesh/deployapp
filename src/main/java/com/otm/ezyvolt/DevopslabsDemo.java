package com.otm.ezyvolt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopslabsDemo {

    public static void main(String args[]) {

        SpringApplication.run(DevopslabsDemo.class, args);


    }

}
