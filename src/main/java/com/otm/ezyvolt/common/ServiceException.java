package com.otm.ezyvolt.common;


public class ServiceException extends Exception {

    String type;
    String message;


   public ServiceException(String type,String msg ){
        this.type=type;
        this.message=msg;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
