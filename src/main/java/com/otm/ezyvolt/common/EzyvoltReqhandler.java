package com.otm.ezyvolt.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Configuration
public class EzyvoltReqhandler extends HandlerInterceptorAdapter {

    private final static Logger LOG = LoggerFactory.getLogger(EzyvoltReqhandler.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String postData;
        HttpServletRequest requestCacheWrapperObject = null;
        //LOG.info("REQ :"+ request.getRequestURI());
        HttpSession session =request.getSession();
        if(session.getAttribute("USER")==null && !request
                .getRequestURI().contains("SignIn")&& !request.getRequestURI
                ().endsWith("login")&& !request.getRequestURI().contains("img") &&!request.getRequestURI().contains("swagger") && !request.getRequestURI().contains("css") && !request.getRequestURI
                ().contains("resources") && !request.getRequestURI().contains("api") && !request.getRequestURI().contains("error") && !request.getRequestURI().contains("js")){

            response.sendRedirect("/login");
            return false;
        }


        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
       // LOG.info("RESPONSE: " + response.getStatus());
    }
}
