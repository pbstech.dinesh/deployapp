package com.otm.ezyvolt.common;

public class AppException  extends Exception {

    public AppException(String msg){
        super(msg);
    }
    public AppException(Throwable t, String msg){
        super(t);
    }


    public AppException(String error, String errorMg) {
        super(errorMg);

    }
}
