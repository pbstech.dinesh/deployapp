package com.otm.ezyvolt.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ReqConfig  implements WebMvcConfigurer {

    @Autowired
    EzyvoltReqhandler reqhandler;



    @Override
    public void addInterceptors(InterceptorRegistry registry){
        registry.addInterceptor(reqhandler);
    }




}
