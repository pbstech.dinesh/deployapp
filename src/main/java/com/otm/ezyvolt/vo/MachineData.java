package com.otm.ezyvolt.vo;

import java.util.Date;
import java.util.List;

public class MachineData implements Comparable<MachineData> {

    private int count;
    private Date dateTime;
    private List<String> machines;
    private int timeInteval;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public List<String> getMachines() {
        return machines;
    }

    public int getTimeInteval() {
        return timeInteval;
    }

    public void setTimeInteval(int timeInteval) {
        this.timeInteval = timeInteval;
    }

    @Override
    public int compareTo(MachineData o) {
        if (getDateTime() == null || o.getDateTime() == null)
            return 0;
        return getDateTime().compareTo(o.getDateTime());
    }
}
