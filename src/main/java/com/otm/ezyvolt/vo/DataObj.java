package com.otm.ezyvolt.vo;

public class DataObj {
    //Machine ID
    private String mcid;
    //Voltage phase RY
    private String vry;
    //Voltage Phase YB
    private String vyb;
    //voltage phase R
    private String IR;
    // voltage phase Y
    private String IY;
    //voltage phase B
    private String IB;
    // Speed in mm/s
    private String spd;
    // last 5 seconds cnt
    private String cnt;


    public String getMcid() {
        return mcid;
    }

    public void setMcid(String mcid) {
        this.mcid = mcid;
    }

    public String getVry() {
        return vry;
    }

    public void setVry(String vry) {
        this.vry = vry;
    }

    public String getVyb() {
        return vyb;
    }

    public void setVyb(String vyb) {
        this.vyb = vyb;
    }

    public String getIR() {
        return IR;
    }

    public void setIR(String IR) {
        this.IR = IR;
    }

    public String getIY() {
        return IY;
    }

    public void setIY(String IY) {
        this.IY = IY;
    }

    public String getIB() {
        return IB;
    }

    public void setIB(String IB) {
        this.IB = IB;
    }

    public String getSpd() {
        return spd;
    }

    public void setSpd(String spd) {
        this.spd = spd;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String toString(){
        return "speed :" + this.getSpd();
    }
}
