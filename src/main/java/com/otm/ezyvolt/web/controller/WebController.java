package com.otm.ezyvolt.web.controller;

import com.otm.ezyvolt.common.ServiceException;
import com.otm.ezyvolt.common.UserForm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class WebController {

    Logger LOG= LoggerFactory.getLogger(WebController.class);


    @GetMapping("/")
    public String page1() {
        return "pages/live";
    }

    @PostMapping("/SignIn")
    @ResponseBody
    public String login(HttpServletRequest request, HttpServletResponse response, UserForm form){

        LOG.info("Registering user account with information: {}", form);


            request.getSession().setAttribute("USER",form);
             return "SUCCESS";




    }


    @GetMapping("/login")
    public String doLogin(){
        return "pages/login";
    }

    @GetMapping("/users")
    public String users() {
        return "pages/Users";
    }

    @GetMapping("/users/new")
    public String newUser() {
        return "pages/addUser";
    }


    @GetMapping("/users/edit")
    public String editUser() {
        return "pages/editUser";
    }

    @GetMapping("/users/changePass")
    public String changePass() {
        return "pages/changePass";
    }
    //Machine Configuration
    @GetMapping("/machines")
    public String pages() {
        return "pages/machines";
    }

    @GetMapping("/machines/new")
    public String addMachine(){
        return "pages/addMachine";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse respons){
        request.getSession(false).removeAttribute("USER");
        request.getSession(false).invalidate();
        LOG.info("SESSION invalidated");
        return "pages/login";
    }




}
